position_list = []
texture_list = []
normal_list = []

triangle_list = []

with open("sphere.obj", "r") as infile:
    for line in infile:
        line = line.strip()

        if line.startswith("v "):
            tokens = line.split()
            position_list.append((float(tokens[1]), float(tokens[2]), float(tokens[3])))
            continue

        if line.startswith("vt "):
            tokens = line.split()
            texture_list.append((float(tokens[1]), float(tokens[2])))
            continue

        if line.startswith("vn "):
            tokens = line.split()
            normal_list.append((float(tokens[1]), float(tokens[2]), float(tokens[3])))
            continue

        if line.startswith("f "):
            tokens = line.split()

            vertex1_indices = tokens[1].split("/")
            vertex1 = (position_list[int(vertex1_indices[0]) - 1], texture_list[int(vertex1_indices[1]) - 1], normal_list[int(vertex1_indices[2]) - 1])

            vertex2_indices = tokens[2].split("/")
            vertex2 = (position_list[int(vertex2_indices[0]) - 1], texture_list[int(vertex2_indices[1]) - 1], normal_list[int(vertex2_indices[2]) - 1])

            vertex3_indices = tokens[3].split("/")
            vertex3 = (position_list[int(vertex3_indices[0]) - 1], texture_list[int(vertex3_indices[1]) - 1], normal_list[int(vertex3_indices[2]) - 1])

            triangle_list.append((vertex1, vertex2, vertex3))
            continue

for (vertex1, vertex2, vertex3) in triangle_list:
    ((x, y, z), (u, v), (nx, ny, nz)) = vertex1
    print("{0} {1} {2} {3} {4} {5} {6} {7}".format(x, y, z, u, v, nx, ny, nz))

    ((x, y, z), (u, v), (nx, ny, nz)) = vertex2
    print("{0} {1} {2} {3} {4} {5} {6} {7}".format(x, y, z, u, v, nx, ny, nz))

    ((x, y, z), (u, v), (nx, ny, nz)) = vertex3
    print("{0} {1} {2} {3} {4} {5} {6} {7}".format(x, y, z, u, v, nx, ny, nz))

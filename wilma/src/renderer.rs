use std::ffi::{ CString, CStr };

extern crate gl;

pub struct Shader {
  id : gl::types::GLuint,
}

impl Shader {
  fn from_source(source: &CStr, kind: gl::types::GLenum) -> Result<Self, String> {
    println!("Creating gl shader...");

    let id = unsafe { gl::CreateShader(kind) };

    unsafe {
      gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
      gl::CompileShader(id);
    }

    let mut success: gl::types::GLint = 1;

    unsafe {
      gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
    }

    if success == 0 {
      let mut len: gl::types::GLint = 0;

      unsafe {
        gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
      }

      let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
      buffer.extend([b' '].iter().cycle().take(len as usize));

      let error = unsafe { CString::from_vec_unchecked(buffer) };

      unsafe {
        gl::GetShaderInfoLog(
          id,
          len,
          std::ptr::null_mut(),
          error.as_ptr() as *mut gl::types::GLchar
        );
      }

      return Err(error.to_string_lossy().into_owned());
    }

    Ok(Self {
      id: id
    })
  }

  pub fn from_vertex_source(source: &CStr) -> Result<Self, String> {
    Self::from_source(source, gl::VERTEX_SHADER)
  }

  pub fn from_fragment_source(source: &CStr) -> Result<Self, String> {
    Self::from_source(source, gl::FRAGMENT_SHADER)
  }
}

impl Shader {
  fn get_id(&self) -> gl::types::GLuint {
    self.id
  }
}

impl std::ops::Drop for Shader {
  fn drop(&mut self) {
    println!("Destroying gl shader...");

    unsafe {
      gl::DeleteShader(self.id);
    }
  }
}

pub struct Program {
  id: gl::types::GLuint,
}

impl Program {
  pub fn from_shaders(shaders: &[Shader]) -> Result<Program, String> {
    println!("creating gl program...");

    let id = unsafe { gl::CreateProgram() };

    for shader in shaders {
      unsafe { gl::AttachShader(id, shader.get_id()); }
    }

    unsafe { gl::LinkProgram(id); }

    let mut success: gl::types::GLint = 1;

    unsafe {
      gl::GetProgramiv(id, gl::LINK_STATUS, &mut success);
    }

    if success == 0 {
      let mut len: gl::types::GLint = 0;

      unsafe {
        gl::GetProgramiv(id, gl::INFO_LOG_LENGTH, &mut len);
      }

      let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
      buffer.extend([b' '].iter().cycle().take(len as usize));

      let error = unsafe { CString::from_vec_unchecked(buffer) };

      unsafe {
        gl::GetProgramInfoLog(
          id,
          len,
          std::ptr::null_mut(),
          error.as_ptr() as *mut gl::types::GLchar
        );
      }

      return Err(error.to_string_lossy().into_owned());
    }

    for shader in shaders {
      unsafe { gl::DetachShader(id, shader.get_id()); }
    }

    Ok(Program { id: id })
  }
}

impl Program {
  pub fn get_id(&self) -> gl::types::GLuint {
    self.id
  }

  pub fn set_used(&self) {
    unsafe {
      gl::UseProgram(self.id)
    }
  }
}

impl Drop for Program {
  fn drop(&mut self) {
    println!("Destroying gl program...");

    unsafe {
      gl::DeleteProgram(self.id);
    }
  }
}

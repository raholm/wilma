#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 InputColor;

layout(location = 0) out vec4 OutputColor;

void main()
{
  OutputColor = vec4(0.71f, 0.5f, 0.2f, 1.0f);
  OutputColor = vec4(InputColor, 1.0f);
}

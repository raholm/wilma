#version 330 core
#extension GL_ARB_separate_shader_objects : enable

uniform mat4 Perspective;
uniform mat4 View;
uniform sampler2D PerlinNoise;

layout(location = 0) in vec3 Position;
layout(location = 1) in vec2 Texture;
layout(location = 2) in vec3 Normal;

layout(location = 0) out vec3 OutputColor;

void main()
{
  float Noise = texture(PerlinNoise, Texture).r;

  mat4 Model = mat4(1.0);
  Model[0][0] = 3.0;
  Model[1][1] = 2.0;
  Model[2][2] = 2.0;

  gl_Position = Perspective * View * Model * vec4(Position + smoothstep(0.0, 1.0, sin(sqrt(abs(Noise))) / 2.0) * Normal, 1.0);
  OutputColor = abs(vec3(Noise, Noise, Noise));
}

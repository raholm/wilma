use std::ffi::CString;
use std::os::raw::c_void;
use std::fs::File;
use std::io::{ BufRead, BufReader };

extern crate sdl2;
extern crate gl;
extern crate cgmath;
extern crate rand;

use cgmath::Matrix;
use rand::Rng;

pub mod renderer;

fn lerp(x: f32, y: f32, w: f32) -> f32 {
  (y - x) * w + x
}

fn random_gradient(x: i32, y: i32) -> (f32, f32) {
  let w = 8 * 4;
  let s = w / 2;

  let a = x as u32;
  let mut b = y as u32;

  let (mut a, _) = a.overflowing_mul(3284157443);
  b ^= a << s | a >> (w - s);
  let (b, _) = b.overflowing_mul(1911520717);
  a ^= b << s | b >> (w - s);
  let (a, _) = a.overflowing_mul(2048419325);

  let random = a as f32 * (3.14159265 / (!(!0 as u32 >> 1)) as f32);
  let result = (random.sin(), random.cos());

  result
}

fn dot_grid_gradient(ix: i32, iy: i32, x: f32, y: f32) -> f32 {
  let (gx, gy) = random_gradient(ix, iy);

  let dx = x - ix as f32;
  let dy = y - iy as f32;

  dx * gx + dy * gy
}

fn perlin(x: f32, y: f32) -> f32 {
  let x0 = x as i32;
  let x1 = x0 + 1;

  let y0 = y as i32;
  let y1 = y0 + 1;

  let wx = x - x0 as f32;
  let wy = y - y0 as f32;

  let n0 = dot_grid_gradient(x0, y0, x, y);
  let n1 = dot_grid_gradient(x1, y0, x, y);
  let ix0 = lerp(n0, n1, wx);

  let n0 = dot_grid_gradient(x0, y1, x, y);
  let n1 = dot_grid_gradient(x1, y1, x, y);
  let ix1 = lerp(n0, n1, wx);

  lerp(ix0, ix1, wy)
}

fn octave_perlin(x: f32, y:f32) -> f32 {
  let octaves = 4;
  let persistence = 0.4;

  let mut frequency = 1.0;
  let mut amplitude = 1.0;
  let mut max_value = 0.0;
  let mut total = 0.0;

  for _ in 0..octaves {
    total += perlin(x * frequency, y * frequency) * amplitude;
    max_value += amplitude;

    amplitude *= persistence;
    frequency *= 2.0;
  }

  total / max_value
}

const TEXTURE_HEIGHT: usize = 256;
const TEXTURE_WIDTH: usize = 256;

fn main() {
  println!("Welcome to wilma!");

  let sdl = sdl2::init().unwrap();
  let video_subsystem = sdl.video().unwrap();
  let gl_attr = video_subsystem.gl_attr();
  gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
  gl_attr.set_context_version(4, 6);

  let window = video_subsystem
    .window("Wilma", 1920, 1080)
    .opengl()
    .build()
    .unwrap();
  let _gl_context = window.gl_create_context().unwrap();
  let _gl = gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

  let shader_program = {
    let vertex_shader = renderer::Shader::from_vertex_source(
      &CString::new(include_str!("sphere.vert")).unwrap()
    ).unwrap();

    let fragment_shader = renderer::Shader::from_fragment_source(
      &CString::new(include_str!("sphere.frag")).unwrap()
    ).unwrap();

    renderer::Program::from_shaders(&[vertex_shader, fragment_shader]).unwrap()
  };

  let file = File::open("../resources/sphere.dat").unwrap();
  // let file = File::open("../resources/sphere_small.dat").unwrap();
  let reader = BufReader::new(file);

  let mut sphere_vertices: Vec<f32> = Vec::new();

  for line in reader.lines() {
    let line = line.unwrap();
    let floats: Vec<f32> = line.split(" ").filter_map(|s| s.parse::<f32>().ok()).collect();

    sphere_vertices.extend(&floats);
  }

  let mut vbo: gl::types::GLuint = 0;

  unsafe {
    gl::GenBuffers(1, &mut vbo);
  }

  unsafe {
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    gl::BufferData(gl::ARRAY_BUFFER,
                   (sphere_vertices.len() * std::mem::size_of::<f32>()) as gl::types::GLsizeiptr,
                   sphere_vertices.as_ptr() as *const gl::types::GLvoid,
                   gl::STATIC_DRAW);
    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
  }

  let mut vao: gl::types::GLuint = 0;

  unsafe {
    gl::GenVertexArrays(1, &mut vao);
  }

  let mut texture: gl::types::GLuint = 0;

  unsafe {
    gl::GenTextures(1, &mut texture);
    gl::BindTexture(gl::TEXTURE_2D, texture);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
    gl::BindTexture(gl::TEXTURE_2D, 0);
  }

  let position_offset = 0;
  let texture_offset = 12;
  let normal_offset = 20;

  unsafe {
    gl::BindVertexArray(vao);
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    gl::EnableVertexAttribArray(0);
    gl::VertexAttribPointer(0,
                            3,
                            gl::FLOAT,
                            gl::FALSE,
                            (8 * std::mem::size_of::<f32>()) as gl::types::GLint,
                            position_offset as *const c_void);
    gl::EnableVertexAttribArray(1);
    gl::VertexAttribPointer(1,
                            2,
                            gl::FLOAT,
                            gl::FALSE,
                            (8 * std::mem::size_of::<f32>()) as gl::types::GLint,
                            texture_offset as *const c_void);
    gl::EnableVertexAttribArray(2);
    gl::VertexAttribPointer(2,
                            3,
                            gl::FLOAT,
                            gl::FALSE,
                            (8 * std::mem::size_of::<f32>()) as gl::types::GLint,
                            normal_offset as *const c_void);
    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    gl::BindVertexArray(0);
  }

  let (window_width, window_height) = window.drawable_size();

  let perspective_matrix = cgmath::perspective(cgmath::Deg(75.0), window_width as f32 / window_height as f32, 0.001, 100.0);
  let view_matrix = cgmath::Matrix4::<f32>::from_translation(cgmath::Vector3::new(0.0, 0.0, -2.0));

  unsafe {
    gl::Enable(gl::DEPTH_TEST);
  }

  let perspective_string = CString::new("Perspective").unwrap();
  let view_string = CString::new("View").unwrap();
  let perlin_noise_string = CString::new("PerlinNoise").unwrap();

  let mut rng = rand::thread_rng();
  let mut perlin_noise = [0.0; TEXTURE_WIDTH * TEXTURE_HEIGHT];

  unsafe {
    gl::BindTexture(gl::TEXTURE_2D, texture);
    gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RED as i32, TEXTURE_WIDTH as i32, TEXTURE_HEIGHT as i32, 0, gl::RED, gl::FLOAT, perlin_noise.as_ptr() as *const gl::types::GLvoid);
    gl::BindTexture(gl::TEXTURE_2D, 0);
  }

  let mut event_pump = sdl.event_pump().unwrap();

  'main: loop {
    for event in event_pump.poll_iter() {
      match event {
        sdl2::event::Event::Quit {..} => break 'main,
        sdl2::event::Event::KeyUp {
          keycode, ..
        } => {
          if let Some(keycode) = keycode {
            match keycode {
              sdl2::keyboard::Keycode::Escape => break 'main,
              _ => {},
            }
          }
        },
        _ => {},
      }
    }

    for width in 0..TEXTURE_WIDTH {
      for height in 0..TEXTURE_HEIGHT {
        perlin_noise[width * TEXTURE_HEIGHT + height] = octave_perlin(width as f32 + rng.gen::<f32>(), height as f32 + rng.gen::<f32>());
      }
    }

    unsafe {
      gl::BindTexture(gl::TEXTURE_2D, texture);
      gl::TexSubImage2D(gl::TEXTURE_2D, 0, 0, 0, TEXTURE_WIDTH as i32, TEXTURE_HEIGHT as i32, gl::RED, gl::FLOAT, perlin_noise.as_ptr() as *const gl::types::GLvoid);
      gl::BindTexture(gl::TEXTURE_2D, 0);
    }

    shader_program.set_used();

    unsafe {
      let perspective_location = gl::GetUniformLocation(shader_program.get_id(), perspective_string.as_ptr());
      let view_location = gl::GetUniformLocation(shader_program.get_id(), view_string.as_ptr());
      let perlin_noise_location = gl::GetUniformLocation(shader_program.get_id(), perlin_noise_string.as_ptr());

      gl::UniformMatrix4fv(perspective_location, 1, 0, perspective_matrix.as_ptr() as *const gl::types::GLfloat);
      gl::UniformMatrix4fv(view_location, 1, 0, view_matrix.as_ptr() as *const gl::types::GLfloat);
      gl::Uniform1i(perlin_noise_location, gl::TEXTURE0 as i32);

      gl::Viewport(0, 0, window_width as i32, window_height as i32);
      gl::ClearColor(0.1, 0.21, 0.41, 1.0);
      gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

      gl::ActiveTexture(gl::TEXTURE0);
      gl::BindTexture(gl::TEXTURE_2D, texture);
      gl::BindVertexArray(vao);
      gl::DrawArrays(gl::TRIANGLES, 0, (sphere_vertices.len() / 8) as i32);
    }

    window.gl_swap_window();
  }
}
